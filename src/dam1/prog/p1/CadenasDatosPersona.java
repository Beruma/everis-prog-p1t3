package dam1.prog.p1;
/*
 * Se pretende realizar un programa que pida al usuario su nombre, primer apellido y segundo apellido. Lo que queremos
 * es que a partir de esos tres datos podamos realizar las siguientes operaciones haciendo manejo de las funciones
 * pertenecientes al objeto cadena y haciendo un menú que nos permita responder las siguientes cuestiones.
 * 1) ¿Tiene el usuario el primer apellido igual que el segundo?
 * 2) ¿Cuál es la primera letra de su nombre?
 * 3) Sacar la abreviatura de su nombre y apellidos en el siguiente formato: NA1A2, donde N es la primera letra del
 * nombre, A1 la primera del primer apellido y A2 la primera del segundo apellido.
 * 4) ¿Cuántos caracteres tiene su nombre?
 * 5) ¿Cuántos caracteres tiene su nombre y apellidos?
 * 6) Componer un email que sean las tres primeras letras del nombre, las tres letras del medio del apellido1 y las tres
 * letras del final del apellido2. Una vez compuesta esa cadena añadir “@everisschool.es”. Para obtener los caracteres
 * del medio realizaremos el cálculo de dividir la longitud de la cadena entre dos y tomaremos como posición origen el
 * resultado de esa operación.
 * 7) Comprobar si bien el nombre o los apellidos contienen una cadena pedida al usuario. Por ejemplo, si el apellido es
 * Pérez e introduzco la cadena “rez” tiene que devolver el mensaje “cadena contenida”.
 */

import java.util.Scanner;

/**
 * @author Beatriz
 */
public class CadenasDatosPersona {
    static String nombre;
    static String apellido1;
    static String apellido2;
    static Scanner key;

    public static void main(String[] args) {
        key = new Scanner(System.in);
        boolean salir = false;
        int opcion;
        System.out.println("Introduzca su nombre");
        nombre = key.nextLine();
        System.out.println("Introduzca su primer apellido");
        apellido1 = key.nextLine();
        System.out.println("Introduzca su segundo apellido");
        apellido2 = key.nextLine();

        //creamos un menu para poder acceder a cualquiera de los casos//
        while (!salir) {

            System.out.println("1. ¿Tiene el usuario el primer apellido igual que el segundo? ");
            System.out.println("2. ¿Cuál es la primera letra de su nombre?");
            System.out.println("3. Sacar la abreviatura de su nombre y apellidos");
            System.out.println("4. ¿Cuántos caracteres tiene su nombre?");
            System.out.println("5. ¿Cuántos caracteres tiene su nombre y apellidos?");
            System.out.println("6. Componer e-mail");
            System.out.println("7. Comprobar si bien el nombre o los apellidos contienen una cadena");
            System.out.println("8. Salir");
            opcion = key.nextInt();
            switch (opcion) {
                case 1:
                    comprobarApellidos();
                    break;
                case 2:
                    sacarPrimeraLetra();
                    break;
                case 3:
                    abreviarNombre();
                    break;
                case 4:
                    contarNombre();
                    break;
                case 5:
                    contarNombreCompleto();
                    break;
                case 6:
                    componerEmail();
                    break;
                case 7:
                    comprobarCadena();
                    break;
                case 8:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no válida");

            }
        }
    }

    //Comprueba si ambos apellidos son iguales//
    public static void comprobarApellidos() {
        if (apellido1.equalsIgnoreCase(apellido2)) {
            System.out.println("Si, los apellidos son iguales");
        } else {
            System.out.println("No, los apellidos son diferentes");
        }
    }

    //Imprime la primera letra del nombre//
    public static void sacarPrimeraLetra() {

        System.out.println(nombre.substring(0, 1));
    }

    //Imprime la abreviacion del nombre usando la primera letra del nombre y de los apellidos//
    public static void abreviarNombre() {
        System.out.println(nombre.substring(0, 1) + apellido1.substring(0, 1) + apellido2.substring(0, 1));
    }

    //Te dice cuantos caracteres tiene tu nombre//
    public static void contarNombre() {

        System.out.println(nombre.length());
    }

    //Imprime cuantos caracteres tienen la suma de tu nombre con tus apellidos//
    public static void contarNombreCompleto() {
        System.out.println(nombre.length() + apellido1.length() + apellido2.length());
    }

    //utiliza las tres primeras letras de tu nombre, las tres del medio de tu apellido y las tres ultimas de tu segundo apellido y se las suma al email para componerlo
    public static void componerEmail() {
        int mitadApe = apellido1.length() / 2;

        System.out.println(nombre.substring(0, 3) + apellido1.substring(mitadApe - 1, mitadApe + 2) + apellido2.substring(apellido2.length() - 3) + "@everisschool.es");
    }

    //comprueba si la sucesion de caracteres inbroducidos tienen alguna coincidencia con tu nombre o tus apellidos//
    public static void comprobarCadena() {
        key.nextLine();
        System.out.println("Introduzca una cadena de caracteres");
        String cadena = key.nextLine();

        if (nombre.contains(cadena)) {
            System.out.println("Si hay coincidencia en el nombre");
        } else {
            System.out.println("No hay coincidencia en el nombre");
        }
        if (apellido1.contains(cadena)) {
            System.out.println("Si hay coincidencia en el primer apellido");
        } else {
            System.out.println("No hay coincidencia en el primer apellido");
        }
        if (apellido2.contains(cadena)) {
            System.out.println("Si hay coincidencia en el segundo apellido");
        } else {
            System.out.println("No hay coincidencia en el segundo apellido");
        }
    }
}